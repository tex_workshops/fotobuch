
function readEXIF(_file)

    local f = io.open(_file, "rb")
    local data = {}

    f:seek("set", 4)
    local bytes = f:read(2)
    local datasize = tonumber(string.format("%02X%02X", string.byte(bytes, 1), string.byte(bytes, 2)), 16) - 2
    f:seek("cur", 7)
    bytes = f:read(1)
    f:seek("cur", 6)
    bytes = f:read(2)

    local entrysize = tonumber(string.format("%02X%02X", string.byte(bytes, 2), string.byte(bytes, 1)), 16)
    local sub_ifd_offset = 0;

    while entrysize > 0 do
        local entry_tag = f:read(2)
        local entry_size = f:read(2)
        local entry_type = f:read(4)
        local entry_data_or_offset = f:read(4)
        --ofsset is from 49 49 2A 00...(12 bytes shift)

        -- we only find tag 0x8769, which means Exif SubIFD offset
        if string.byte(entry_tag, 1) == 105 and string.byte(entry_tag, 2) == 135 then
            sub_ifd_offset = 12 + tonumber(string.format("%02X%02X%02X%02X", string.byte(entry_data_or_offset, 4), string.byte(entry_data_or_offset, 3), string.byte(entry_data_or_offset, 2), string.byte(entry_data_or_offset, 1)), 16)
            entry_tag, entry_size, entry_type, entry_data_or_offset = nil
            break
        end

        entrysize = entrysize - 1;
        entry_tag, entry_size, entry_type, entry_data_or_offset = nil
    end

    if not (sub_ifd_offset == 0) then
        f:seek("set", sub_ifd_offset)
        bytes = f:read(2)
        entrysize = string.byte(bytes, 2)*256 + string.byte(bytes, 1)
        local now_offset = 0;

        while entrysize > 0 do
            local entry_tag = f:read(2)
            local entry_size = f:read(2)
            local entry_type = f:read(4)
            local entry_data_or_offset = f:read(4)
            --offset is from 49 49 2A 00...(12 bytes shift)


            -- ISO
            if ( tonumber("8827",16) == (string.byte(entry_tag, 2)*256 + string.byte(entry_tag, 1)) ) then
                 data["iso"] = string.byte(entry_data_or_offset, 1) + string.byte(entry_data_or_offset, 2)*256
            -- Exposure
            elseif ( tonumber("829A",16) == (string.byte(entry_tag, 2)*256 + string.byte(entry_tag, 1)) ) then 
                now_offset = f:seek()
                f:seek("set", 12 + string.byte(entry_data_or_offset, 4)*256^3 + string.byte(entry_data_or_offset, 3)*256^2 + string.byte(entry_data_or_offset, 2)*256 + string.byte(entry_data_or_offset, 1))
                bytes = f:read(4)
                local numerator = string.byte(bytes, 4)*256^3 + string.byte(bytes, 3)*256^2 + string.byte(bytes, 2)*256 + string.byte(bytes, 1)
                bytes = f:read(4)
                local denominator = string.byte(bytes, 4)*256^3 + string.byte(bytes, 3)*256^2 + string.byte(bytes, 2)*256 + string.byte(bytes, 1)
                f:seek("set", now_offset)
                data["exposure"] = string.format("%d", numerator/10).."/"..string.format("%d", denominator/10) -- Might need to be adjusted to your camera, maybe remove the divide by 10
            -- Aperture
            elseif ( tonumber("829D",16) == (string.byte(entry_tag, 2)*256 + string.byte(entry_tag, 1)) ) then -- 0x829a ExposureTime,  2 signed/unsigned long integer, value = a/b
                now_offset = f:seek()
                f:seek("set", 12 + string.byte(entry_data_or_offset, 4)*256^3 + string.byte(entry_data_or_offset, 3)*256^2 + string.byte(entry_data_or_offset, 2)*256 + string.byte(entry_data_or_offset, 1))
                bytes = f:read(4)
                local numerator = string.byte(bytes, 4)*256^3 + string.byte(bytes, 3)*256^2 + string.byte(bytes, 2)*256 + string.byte(bytes, 1)
                bytes = f:read(4)
                local denominator = string.byte(bytes, 4)*256^3 + string.byte(bytes, 3)*256^2 + string.byte(bytes, 2)*256 + string.byte(bytes, 1)
                f:seek("set", now_offset)
                if (denominator == 0 ) then
                    data["aperture"] = "unkown"
                else
                    data["aperture"] = numerator/denominator
                end
            -- EV
            elseif ( tonumber("9204",16) == (string.byte(entry_tag, 2)*256 + string.byte(entry_tag, 1)) ) then
                now_offset = f:seek()
                f:seek("set", 12 + string.byte(entry_data_or_offset, 4)*256^3 + string.byte(entry_data_or_offset, 3)*256^2 + string.byte(entry_data_or_offset, 2)*256 + string.byte(entry_data_or_offset, 1))
                bytes = f:read(4)
                local numerator = tonumber(string.format("%02X%02X%02X%02X", string.byte(bytes, 4), string.byte(bytes, 3), string.byte(bytes, 2), string.byte(bytes, 1)), 16)
                if ( bit32.band(string.byte(bytes, 4), 0x80) == 0x80 ) then
                    numerator = (0-bit32.band(bit32.bnot(numerator-1), 0x7FFFFFFF))
                end
                bytes = f:read(4)
                local denominator = tonumber(string.format("%02X%02X%02X%02X", string.byte(bytes, 4), string.byte(bytes, 3), string.byte(bytes, 2), string.byte(bytes, 1)), 16)
                if ( bit32.band(string.byte(bytes, 4), 0x80) == 0x80 ) then
                    denominator = (0-bit32.band(bit32.bnot(denominator-1), 0x7FFFFFFF))
                end
                f:seek("set", now_offset)
                data["ev"] = numerator/denominator
            end

            entrysize = entrysize - 1;
            entry_tag, entry_size, entry_type, entry_data_or_offset = nil
        end

    end

    io.close(f)
    return data

end